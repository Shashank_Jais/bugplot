package Base_Library;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;


public class Browser_Launching {
	
	public static WebDriver driver;

	public void Browser(String url) {
		
		
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://www.bugplot.com/");
		driver.manage().window().maximize();
	}
	
}