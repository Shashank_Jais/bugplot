import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base_Library.Browser_Launching;
import BugPlot_Pages.SignUp_Flow;


public class SignUpFlow_Test extends Browser_Launching {
	
	  private SignUp_Flow ob;

	@BeforeTest
	public void LanchingBrowser() {
		Browser("https://www.bugplot.com/");	
        ob = new SignUp_Flow(driver);
	}

	@Test(priority = 1)
	public void ClickingOnGettingStartedButton() throws InterruptedException {
		ob.ClickonGetStartedButton();
	}
	
	@Test(priority = 2)
	public void SwitchingWindow() {
		ob.SwitchWindow();
		
	}	
	
	@Test(priority = 3)
	public void ClickingOnSignupButton() throws InterruptedException {
		ob.ClickOnSignupButton();
		
	}
	
	@Test(priority = 4)
	public void EnteringFirstName() {
		ob.EnterFirstName();
	}
	
	@Test(priority = 5)
	public void EnteringLastName() {
		ob.EnterLastName();
	}
	
	@Test(priority = 6)
	public void EnteringWorkEmail() {
		ob.EnterWorkEmail();
	}
	
	@Test(priority = 7)
	public void EnteringOrganization() {
		ob.EnterOrganiZation();
	}
	
	@Test(priority = 8)
	public void ScrollingWindow() throws InterruptedException {
		ob.ScrollWindow();
	}
	
	@Test(priority = 9)
	public void EnteringNewPassword() {
		ob.EnterNewPassword();
	}
	
	@Test(priority = 10)
	public void EnteringConfiemPassword() {
		ob.EnterConfirmPassword();
	}
	
	@Test(priority = 11)
	public void ClickingCheckBox() {
		ob.ClickCheckBox();
	}
	
	@Test(priority = 12)
	public void ClickingOnSignupButtonn() {
		ob.ClickOnSignupClick();
	}
	
	@Test(priority = 13)
	public void LaunchingYopmailBrowser() {
		ob.LaunchingYopmail();
	}
	
	@Test(priority = 14)
	public void EnteringYopmailEmail() {
		ob.EnterYopmailEmail();
	}
	
	@Test(priority = 15)
	public void ClickingYopmailnterButton() throws InterruptedException {
		ob.ClickYopmailEnterButton();
	}
	
	@Test(priority = 16)
	public void ClickingOnVerificationEmail() {
		ob.ClickingVerificationEmail();
	}
	
	@Test(priority = 17)
	public void SwitchingIframe() {
		ob.SwitchIframe();
	}
	
	@Test(priority = 18)
	public void ClickingOnVerificationLink() {
		ob.ClickingVerificationLink();
	}
	
	
}